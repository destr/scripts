__author__ = 'Maksimov'

import os
import sys
from argparse import ArgumentParser


class QrcGen:
    def __init__(self):
        self.opt = None
        self.output = sys.stdout

    def run(self):

        parser = ArgumentParser()
        parser.add_argument('-i', dest='input', required=True, help='Input directory')
        parser.add_argument('-o', dest='output', default='-', help='Output file')
        parser.add_argument('--prefix', dest='prefix', default='', help='qrc prefix')

        self.opt = parser.parse_args()
        if not os.path.exists(self.opt.input):
            raise FileExistsError("Directory not exists")

        if self.opt.output != '-':
            self.output = open(self.opt.output, 'w+')

        paths = list()
        for root, dirs, files in os.walk(self.opt.input):
            for file in files:
                if file.endswith('pro') or file.endswith('qrc'):
                    continue
                paths.append(os.path.relpath(os.path.join(root, file), self.opt.input))

        self.gen(paths)

    def gen(self, paths):
        self._writeline('<RCC>')
        self._writeline('<qresource prefix="/{0}">'.format(self.opt.prefix), 1)

        for path in paths:
            path = '<file>%s</file>' % path.replace('\\', '/')
            self._writeline(path, 2)

        self._writeline('</qresource>', 1)
        self._writeline('</RCC>')

    def _writeline(self, line, tabs=0):
        if tabs:
            print('    '*tabs, end='', file=self.output)

        print(line, end='\n', file=self.output)

if __name__ == "__main__":
    try:
        g = QrcGen()
        g.run()
    except Exception as e:
        print(e)
        exit(1)

