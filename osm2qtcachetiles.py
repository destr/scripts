import os
import shutil
from argparse import ArgumentParser


class TilesCacheConverter:
    def __init__(self):
        self.opt = None

    def run(self):
        parser = ArgumentParser()
        parser.add_argument("--osm-dir", dest="osmdir", required=True, help="Path to osm dir tiles")
        parser.add_argument("--out-dir", dest="outdir", required=True, help="Output directory tiles")
        parser.add_argument("--map-id", dest="mapid", default="1", help="Magic number")
        parser.add_argument("--plugin", dest="plugin", default="osm_100",
                            help="Qt Geoservices plugin id from .json file")

        self.opt = parser.parse_args()

        self.__copy_tiles()

    def __copy_tiles(self):

        if not os.path.exists(self.opt.osmdir):
            raise FileNotFoundError(self.opt.osmdir)

        if not os.path.exists(self.opt.outdir):
            os.makedirs(self.opt.outdir)

        zoomdirs = [n for n in os.listdir(self.opt.osmdir) if os.path.isdir(os.path.join(self.opt.osmdir, n))]
        for zoomdir in zoomdirs:
            zp = os.path.join(self.opt.osmdir, zoomdir)
            dstzp = os.path.join(self.opt.outdir, zoomdir)
            xdirs = [n for n in os.listdir(zp) if os.path.isdir(os.path.join(zp, n))]
            if not os.path.exists(dstzp):
                os.mkdir(dstzp)
            for xdir in xdirs:
                xp = os.path.join(zp, xdir)
                dstxp = os.path.join(dstzp, xdir)
                if not os.path.exists(dstxp):
                    os.mkdir(dstxp)
                yfiles = [f for f in os.listdir(xp) if os.path.isfile(os.path.join(xp, f))]
                for yfile in yfiles:
                    srcfile = os.path.join(self.opt.osmdir, zoomdir, xdir, yfile)
                    # l или h .highDpi
                    filename = "{0}-l-{1}-{2}-{3}-{4}".format(self.opt.plugin, self.opt.mapid, zoomdir, xdir, yfile)
                    dstfile = os.path.join(dstxp, filename)
                    shutil.copyfile(srcfile, dstfile)

if __name__ == "__main__":
    c = TilesCacheConverter()
    c.run()

