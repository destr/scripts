#!/usr/bin/env python
# coding: utf-8
__author__ = 'Maksimov'

import sys
import csv
import datetime

from argparse import ArgumentParser

# Конвертер данных из cvs формата экспортированного с http://apinfo.ru/airports/export.html
# в qml модель


class Converter:
    def __init__(self):
        self.opt = None
        self.output = None

    def run(self):
        parser = ArgumentParser()
        parser.add_argument('-i', dest='input', required=True, help='Input file')
        parser.add_argument('-o', dest='output', help='Output file', default='-')
        parser.add_argument('-n', dest='maxnum', help='Maximum ICAO codes to process', default=-1)

        self.opt = parser.parse_args()

        if self.opt.output == '-':
            self.output = sys.stdout
        else:
            self.output = open(self.opt.output, "w+", encoding='utf-8')

        self._file_header()

        self._parse()

        self._file_footer()

    def _file_header(self):
        f = self.output
        print('// Generated by script', datetime.datetime.now(), file=f)
        print("// Command line: ", ' '.join(sys.argv), file=f)
        print('import QtQuick 2.3', file=f)
        print('ListModel {', file=f)

    def _file_footer(self):
        f = self.output
        print('}', file=f)

    def _parse(self):
        f = self.output

        def property_print(key, value):
            if isinstance(value, str):
                value = '"' + value + '"'
            print("    ", key, ":", value, file=f)

        with open(self.opt.input) as file:
            reader = csv.reader(file, delimiter='|')
            for row in reader:
                if row[7] != 'RU':
                    continue

                print('  ListElement {', file=f)

                # iata_code|icao_code|name_rus|name_eng|city_rus
                # |city_eng|country_rus|iso_code|latitude|longitude|runway_length
                property_print("iata_code", row[0])
                property_print("icao_code", row[1])
                property_print("name_rus", row[2])
                property_print("name_eng", row[3])
                property_print("city_rus", row[4])
                property_print("city_eng", row[5])
                property_print("country_rus", row[6])
                property_print("iso_code", row[7])
                property_print("latitude", float(row[8]))
                property_print("longitude", float(row[9]))
                runway_length = None
                try:
                    runway_length = float(row[10])
                except ValueError:
                    runway_length = 0

                property_print("runway_length", runway_length)

                print('  }', file=f)


if __name__ == "__main__":
    conv = Converter()
    conv.run()

