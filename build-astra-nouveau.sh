#!/bin/bash

ROOT_DIR=$HOME/nouveau
BUILD_DIR=$ROOT_DIR/build
SOURCES_DIR=$ROOT_DIR/sources
DEB_DIR=$ROOT_DIR/deb

REPOSITORY="http://mirror.yandex.ru/astra/frozen/orel/1.11/repository/pool/"

PACKAGES=('main/libd/libdrm/libdrm_2.4.67-1.dsc' 'main/x/xserver-xorg-video-nouveau/xserver-xorg-video-nouveau_1.0.12-1.dsc')

#PBUILDER="http://http.debian.net/debian/pool/main/p/pbuilder/pbuilder_0.215+nmu3.dsc"
#LDP_DOCBOOK="http://http.debian.net/debian/pool/main/l/ldp-docbook-stylesheets/ldp-docbook-stylesheets_0.0.20040321-2.dsc"
#LOCAL_BUILD_DIR=$HOME/local_builds

#function build_deps() 
#{
#  local temp_dir=$1
#  pushd `ls -d */`
#  depends=`dpkg-checkbuilddeps 2>&1| cut -d: -f 3 | sed -e 's/([^)]\+)//g'`
#  echo $depends
#
#  sudo apt-get install -f $depends
#  popd
#}


#function build_ldp_docbook()
#{
#  local temp_dir=`mktemp -d`
#  pushd $temp_dir
#  dget --no-conf -q -x --build $LDP_DOCBOOK
#  popd 
#  mv $temp_dir/*.deb $LOCAL_BUILD_DIR
#  # пакеты не будут настроены, т.к. нет все зависимости 
#  # ещё есть, они придут только с pbuilder
#  sudo dpkg -i $LOCAL_BUILD_DIR/*.deb
#
#  rm -rf $temp_dir
#}

#function build_pbuilder()
#{

 # docbook-xsl ldp-docbook-xsl xsltproc dblatex po4a

 # local  temp_dir=`mktemp -d`
 # pushd $temp_dir
 # dget --no-conf -q --extract $PBUILDER
 # build_deps
#
#  popd
#}

#xsltproc libx11-dev libpthread-stubs0-dev libpciaccess-dev valgrind
# xserver-xorg-dev (>= 2:1.9.4) mesa-common-dev
function download_sources() 
{
  if [[ ! -d $SOURCES_DIR ]]; then
    mkdir -p $SOURCES_DIR
  fi

  pushd $SOURCES_DIR

  for ((i=0; i < ${#PACKAGES[*]}; i++))
  do
    url=${REPOSITORY}${PACKAGES[$i]}
    dget --no-conf -q  $url
#    sudo dpkg -i *.deb
#    find . -maxdepth 1 -type f -print0 | xargs -I {} -0 mv {} $LOCAL_BUILD_DIR
#    break
  done

  popd
}

function install_depends() {
  sudo apt-get install -y devscripts build-essential quilt
  sudo apt-get install -y dh-autoreconf xsltproc docbook-xsl libx11-dev pkg-config xutils-dev\
     libpthread-stubs0-dev libudev-dev libpciaccess-dev valgrind

  sudo apt-get install -y xserver-xorg-dev x11proto-video-dev x11proto-fonts-dev x11proto-randr-dev \
    x11proto-render-dev x11proto-xext-dev x11proto-dri2-dev x11proto-gl-dev mesa-common-dev

}

function build_sources()
{
  #if [[ -d $BUILD_DIR ]]; then
    #rm -rf $BUILD_DIR
  #fi

  mkdir -p $BUILD_DIR
  test ! -d $DEB_DIR && mkdir -p $DEB_DIR
  pushd $BUILD_DIR

  for ((i=1; i < ${#PACKAGES[*]}; i++))
  do
    dscfile=$(basename ${PACKAGES[$i]})
    dstname=$(basename $dscfile .dsc)
    dscfile=$SOURCES_DIR/$dscfile
    dpkg-source --no-copy -x $dscfile $dstname
    
    pushd $dstname
    dpkg-buildpackage -rfakeroot -b -uc
    popd

    sudo dpkg -i *.deb

    mv *.deb $DEB_DIR
    mv *.udeb $DEB_DIR

  done
  
  popd
}



function usage()
{
  echo "Usage: $0"
  echo "    -B - Only build from sources dir ${SOURCES_DIR}"
  echo "    -D - Only download to souces dir ${SOURCES_DIR}"
  echo "    -i - Install dependencies"
}

build_flag=false
download_flag=false

while getopts "BDhi" o; do
  case "${o}" in
    B)
      build_sources
      ;;
    D)
      download_flag=true
      ;;
    i)
      install_depends
      ;;
    *)
      usage
      ;;
  esac
done
$download_flag && download_sources

