__author__ = 'Maksimov'

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QWidget, QDialog
from ui_codegen import Ui_Codegen


class PropertyGenerator:
    def __init__(self, cpp_fields, classname):
        self.input = cpp_fields
        self.property = ''
        self.signals = ''
        self.setters = ''
        self.getters = ''
        self.assigns = ''

        self.setters_impl = ''
        self.getters_impl = ''

        self.classname = classname


    def run(self):
        lines = self.input.splitlines()
        fields = list()
        for line in lines:
            line = line.strip()
            if line.startswith('/'):
                continue
            val = line.split(';')[0]
            val = val.strip()

            if ' ' in val:
                (type, value) = val.split(' ', 2)
                fields.append((type, value))

        for type, value in fields:

            self.property += "Q_PROPERTY({0} {1} READ {1} WRITE set_{1} NOTIFY {1}Changed)\n".format(type, value)
            self.assigns += "set_{0}(data.{0});\n".format(value)
            self.setters += "void set_{1}({0} {1});\n".format(type, value)
            self.getters += "{0} {1}() const;\n".format(type, value)
            self.signals += "void {1}Changed({0} value);\n".format(type, value)

            self.setters_impl += 'void {0}::set_{2}({1} {2}) {{\n' \
                                 '    if (data_.{2} != {2}) {{\n' \
                                 '        data_.{2} = {2};\n' \
                                 '        emit {2}Changed(data_.{2});\n' \
                                 '    }}  // set_{2}\n' \
                                 '}}\n'.format(self.classname, type, value)

            self.getters_impl += '{1} {0}::{2}() const {{ \n' \
                                 '    return data_.{2};\n' \
                                 '}}  // {2}\n'.format(self.classname, type, value)


class Codegen(QDialog):
    def __init__(self, parent=None):
        super(Codegen, self).__init__(parent)

        self.ui = Ui_Codegen()
        self.ui.setupUi(self)
        self.ui.pushButtonGenerate.clicked.connect(self.generate)
        self.ui.pushButtonClose.clicked.connect(self.close)

    def generate(self):
        print("generate")
        pg = PropertyGenerator(self.ui.plainTextEditInput.toPlainText(), self.ui.lineEditClassName.text())
        pg.run()

        self.ui.plainTextEditProperty.setPlainText(pg.property)
        self.ui.plainTextEditSetters.setPlainText(pg.setters)
        self.ui.plainTextEditGetters.setPlainText(pg.getters)
        self.ui.plainTextEditSignals.setPlainText(pg.signals)
        self.ui.plainTextEditMembers.setPlainText(pg.assigns)
        self.ui.plainTextEditSettersImpl.setPlainText(pg.setters_impl)
        self.ui.plainTextEditGettersImpl.setPlainText(pg.getters_impl)


if __name__ == "__main__":
    import sys
    def my_excepthook(type, value, tback):
        # log the exception here

        # then call the default handler
        sys.__excepthook__(type, value, tback)

    sys.excepthook = my_excepthook
    try:
        app = QApplication(sys.argv)
        codegen = Codegen()
        codegen.show()
        app.exec_()
    except Exception as e:
        print(e)
