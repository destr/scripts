# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Maksimov\Projects\scripts\codegen\codegen.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Codegen(object):
    def setupUi(self, Codegen):
        Codegen.setObjectName("Codegen")
        Codegen.resize(983, 940)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Codegen)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Codegen)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.lineEditClassName = QtWidgets.QLineEdit(Codegen)
        self.lineEditClassName.setObjectName("lineEditClassName")
        self.horizontalLayout_2.addWidget(self.lineEditClassName)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.splitter = QtWidgets.QSplitter(Codegen)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.plainTextEditInput = QtWidgets.QPlainTextEdit(self.splitter)
        self.plainTextEditInput.setObjectName("plainTextEditInput")
        self.scrollArea = QtWidgets.QScrollArea(self.splitter)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 498, 857))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.plainTextEditSetters = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditSetters.setObjectName("plainTextEditSetters")
        self.verticalLayout.addWidget(self.plainTextEditSetters)
        self.plainTextEditMembers = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditMembers.setObjectName("plainTextEditMembers")
        self.verticalLayout.addWidget(self.plainTextEditMembers)
        self.plainTextEditSignals = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditSignals.setObjectName("plainTextEditSignals")
        self.verticalLayout.addWidget(self.plainTextEditSignals)
        self.plainTextEditGettersImpl = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditGettersImpl.setObjectName("plainTextEditGettersImpl")
        self.verticalLayout.addWidget(self.plainTextEditGettersImpl)
        self.plainTextEditGetters = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditGetters.setReadOnly(True)
        self.plainTextEditGetters.setObjectName("plainTextEditGetters")
        self.verticalLayout.addWidget(self.plainTextEditGetters)
        self.plainTextEditProperty = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditProperty.setObjectName("plainTextEditProperty")
        self.verticalLayout.addWidget(self.plainTextEditProperty)
        self.plainTextEditSettersImpl = QtWidgets.QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEditSettersImpl.setObjectName("plainTextEditSettersImpl")
        self.verticalLayout.addWidget(self.plainTextEditSettersImpl)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout_2.addWidget(self.splitter)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButtonGenerate = QtWidgets.QPushButton(Codegen)
        self.pushButtonGenerate.setObjectName("pushButtonGenerate")
        self.horizontalLayout.addWidget(self.pushButtonGenerate)
        self.pushButtonClose = QtWidgets.QPushButton(Codegen)
        self.pushButtonClose.setObjectName("pushButtonClose")
        self.horizontalLayout.addWidget(self.pushButtonClose)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.verticalLayout_2.setStretch(1, 1)

        self.retranslateUi(Codegen)
        QtCore.QMetaObject.connectSlotsByName(Codegen)

    def retranslateUi(self, Codegen):
        _translate = QtCore.QCoreApplication.translate
        Codegen.setWindowTitle(_translate("Codegen", "Dialog"))
        self.label.setText(_translate("Codegen", "Class name"))
        self.pushButtonGenerate.setText(_translate("Codegen", "Generate"))
        self.pushButtonClose.setText(_translate("Codegen", "Close"))

