import hashlib
import os
import sys
from argparse import ArgumentParser

class Md5Sum:
    def __init__(self):
        self.opt=None
        self.outfile=None

    def run(self):

        parser = ArgumentParser()
        parser.add_argument("-o", dest='output', default='-', help='Output file. Default stdout')
        parser.add_argument("dir", nargs='?', default='.')

        self.opt = parser.parse_args()
        if self.opt.output == "-":
            self.outfile = sys.stdout
        else:
            self.outfile = open(self.opt.output, "w", encoding='utf-8')

        path = self.opt.dir
        for root, dirs, files in os.walk(path):
            for file in files:
                filename = os.path.join(root, file)

                sum = self.md5sum(filename)
                print("{0} {1}".format(sum, os.path.relpath(filename, path)), file=self.outfile)

    def md5sum(self, filename, blocksize=65536):
        hash = hashlib.sha256()
        with open(filename, "rb") as f:
            for block in iter(lambda: f.read(blocksize), b""):
                hash.update(block)
        return hash.hexdigest()

if __name__ == "__main__":
    m = Md5Sum()
    m.run()