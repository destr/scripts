from xml.etree.ElementTree import ElementTree
import xml.etree.ElementTree as ET
from lxml import etree as EL
from argparse import ArgumentParser
import os
import sys
import hashlib
import datetime


def increment_version(text_version):
    version_numbers = [int(i) for i in text_version.split('.')]
    version_numbers[-1] += 1
    return ".".join(str(v) for v in version_numbers)


class Configuration:
    def __init__(self, opt):
        self.opt = opt
        self.remote_path = {
            'develop': 'file:///U:/Матюшенко/_Проекты/Брифинг (Туполев и СЛО)/Рабочая папка/Инсталятор/repository',
            'production': 'file:///C:/Briefing_repository'
        }

    def run(self):
        if self.opt.configtype == "version":
            self.__find_files()
        elif self.opt.configtype == "remote":
            self.__change_remote()

    def __change_remote(self):
        if self.opt.remotetype not in self.remote_path:
            raise ValueError("Unknown remote type")

        tree = ElementTree()
        filename = os.path.join(self.opt.packages_dir, "..\config\config.xml")
        tree.parse(filename)
        url_tag = tree.find("RemoteRepositories/Repository/Url")
        url_tag.text = self.remote_path[self.opt.remotetype]

        tree.write(filename, xml_declaration=True, encoding='utf-8')

    def __find_files(self):
        for dir in os.listdir(self.opt.packages_dir):
            if self.opt.modules:
                if dir not in self.opt.modules:
                    continue

            xml_file = os.path.join(self.opt.packages_dir, dir, "meta", "package.xml")
            self.__change_version(xml_file)

        self.__change_version(os.path.join(self.opt.packages_dir, "../config/config.xml"))

    def __change_version(self, filename):
        tree = ElementTree()
        tree.parse(filename)
        version_tag = tree.find("Version")

        release_tag = tree.find("ReleaseDate")
        if release_tag is not None:
            release_tag.text = datetime.datetime.now().strftime("%Y-%m-%d")

        if not self.opt.only_release_date:
            if self.opt.setversion:
                version_tag.text = self.opt.setversion
            else:
                version_tag.text = increment_version(version_tag.text)

        tree.write(filename, xml_declaration=True, encoding='utf-8')


class HashSum:
    def __init__(self, opt):
        self.opt = opt
        self.outfile = None

    def run(self):
        if self.opt.output == "-":
            self.outfile = sys.stdout
        else:
            self.outfile = open(self.opt.output, "w", encoding='utf-8')

        path = self.opt.dir

        if self.opt.hashlib:
            self._print("#include <QtCore/qglobal.h>\n")
            self._print("const char * checksums[] = {\n")

        for root, dirs, files in os.walk(path):
            for file in files:
                if 'checksums' in file:
                    continue
                if file in ('map.ini', 'server.ini'):
                    continue

                filename = os.path.join(root, file)

                sum = self.hashsum(filename)

                format_string = None
                if self.opt.hashlib:
                    format_string = '  "{0} {1}",'
                else:
                    format_string = '{0} {1}'

                self._print(format_string.format(sum, os.path.relpath(filename, path).replace('\\', '/')))

        if self.opt.hashlib:
            self._print("  0")
            self._print("};")
            self._print("#if defined __cplusplus\n"
            "extern \"C\"{\n"
            "#endif\n"
                        "Q_DECL_EXPORT const char **sums() { return checksums; }\n"
            "#if defined __cplusplus\n"
            "}\n"
            "#endif\n"
                        )

    def _print(self, str):
        print(str, file=self.outfile)

    @staticmethod
    def hashsum(filename, blocksize=65536):
        hash = hashlib.sha256()
        with open(filename, "rb") as f:
            for block in iter(lambda: f.read(blocksize), b""):
                hash.update(block)
        return hash.hexdigest()


class Android:
    def __init__(self, opt):
        self.opt = opt
        self.xmlns = {"android": "http://schemas.android.com/apk/res/android"}

    def run(self):
        if self.opt.configtype == "version":
            self.__change_version()
        elif self.opt.configtype == "getversion":
            print(self.__get_version(), end='')

    def __get_version(self):
        tree = self.__parse_manifest()
        manifest = tree.getroot()
        return manifest.get(self.__ns("versionName"))

    def __change_version(self):
        tree = self.__parse_manifest()

        manifest = tree.getroot()
        version_name = manifest.get(self.__ns("versionName"))

        if self.opt.setversion:
            version_name = self.opt.setversion
        else:
            version_name = increment_version(version_name)

        manifest.set(self.__ns("versionName"), version_name)

        version_code = manifest.get(self.__ns("versionCode"))
        manifest.set(self.__ns("versionCode"), increment_version(version_code))

        filename = self.__filename()
        tree.write(filename, xml_declaration=True, encoding='utf-8')

    def __parse_manifest(self):
        EL.register_namespace('anroid', "http://schemas.android.com/apk/res/android")
        filename = self.__filename()

        tree = EL.parse(filename, EL.XMLParser(remove_comments=False))
        return tree

    def __filename(self):
        return os.path.join(self.opt.packages_dir, "../../src/efb/briefingapp/android/AndroidManifest.xml")

    @staticmethod
    def __ns(name):
        return "{http://schemas.android.com/apk/res/android}" + name


class BuildInstallerHelper:
    def __init__(self):
        self.opt = None

    def run(self):
        parser = ArgumentParser()
        parser.add_argument("-o", dest='output', default='-', help='Output file. Default stdout')
        parser.add_argument("dir", nargs='?', default='.')
        parser.add_argument("-p", dest='packages_dir', required=True, help='Packages dir')
        parser.add_argument("-m", dest='modules', action='append', help='Change version in this module')
        parser.add_argument("--helper", dest='helper', required=True, help='Helper type[config, hash, android]')
        parser.add_argument('--config-type', dest='configtype', help='Type changed config. [version, remote]')
        parser.add_argument('--hash-lib', dest='hashlib', action='store_true', help='Output hashes as cpp lib')
        parser.add_argument('--remote-type', dest='remotetype', help='Remote type [develop, production]')
        parser.add_argument('--set-version', dest='setversion', help='Set version')
        parser.add_argument('--only-release-date', dest='only_release_date', action='store_true', help='Change only release date')

        self.opt = parser.parse_args()

        if self.opt.helper == 'config':
            v = Configuration(self.opt)
            v.run()
        elif self.opt.helper == 'hash':
            h = HashSum(self.opt)
            h.run()
        elif self.opt.helper == 'android':
            a = Android(self.opt)
            a.run()
        else:
            raise ValueError("Unknown helper")

if __name__ == "__main__":
    v = BuildInstallerHelper()
    v.run()