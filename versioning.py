from xml.etree.ElementTree import ElementTree
from argparse import ArgumentParser
import os
import datetime


class Versioning:
    def __init__(self):
        self.opt=None

    def run(self):
        parser = ArgumentParser()
        parser.add_argument("-p", dest='packages_dir', required=True, help='Packages dir')
        parser.add_argument("-m", dest='modules', help='Include module', action='append')
        parser.add_argument("--type", dest='type', help=)

        self.opt = parser.parse_args()
        self.__find_files()

    def __find_files(self):
        for dir in os.listdir(self.opt.packages_dir):
            if self.opt.modules:
                if dir not in self.opt.modules:
                    continue

            xml_file = os.path.join(self.opt.packages_dir, dir, "meta", "package.xml")
            self.__change_version(xml_file)

    def __change_version(self, filename):
        tree = ElementTree()
        tree.parse(filename)
        version_tag = tree.find("Version")

        release_tag = tree.find("ReleaseDate")
        release_tag.text = datetime.datetime.now().strftime("%Y-%m-%d")

        version_numbers = [int(i) for i in version_tag.text.split('.')]
        version_numbers[2] += 1

        version_tag.text = ".".join(str(v) for v in version_numbers)
        tree.write(filename, xml_declaration=True, encoding='utf-8')


if __name__ == "__main__":
    v = Versioning()
    v.run()


